module.exports = {
  siteMetadata: {
    title: 'Gatsby Default Starter',
  },
  plugins: ['gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: `x2e2nv6lfd8i`,
        accessToken: `ef1fcd8a3b12ffd00ac340d86d941949d2b3da3eeded00c9c4e859735c8675ca`,
      },
    },],
}
