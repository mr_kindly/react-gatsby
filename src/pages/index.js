import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <h1>Hi people</h1>
    <p>Welcome to your new Gatsby site.</p>
    <p>Now go build something great.</p>
    <Link to="/page-2/">Go to page 2</Link>
  </div>
)

export default IndexPage


// example
// const IndexPage = ({ data }) => {
//   const { allContentfulCourse: { edges: edges } } = data;
//   return <div>
//     {
//       edges.map((edge) => {
//         const node = edge.node;
//         return (<div key={node.id}>
//           <h3>{node.title}</h3>
//           <h4>Duration: {node.duration}</h4>
//           <p><strong>Description: </strong>{node.shortDescription}</p>
//           <hr />
//         </div>);
//       })
//     }
//   </div>
// }


// export default IndexPage

// export const query = graphql`
//   query PageQuery {
// 	allContentfulCourse {
// 	  edges {
// 	    node {
// 	      id,
//         title,
//         shortDescription,
//       	duration
// 	    }
// 	  }
// 	}

// }
// `